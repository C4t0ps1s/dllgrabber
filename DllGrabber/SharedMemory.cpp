#include "SharedMemory.h"



SharedMemory::SharedMemory(){
	hCurrentDll = GetModuleHandle(L"DllGrabber.dll");
	if (!hCurrentDll) {
		l.error(L"SharedMemory::constructor getModuleHandle of DllGrabber");
		return;
	}

	baseAddress = hCurrentDll + 0x80;

	
	if (strncmp((char*)baseAddress, SHARED_MEMORY_CHECK,strlen(SHARED_MEMORY_CHECK))) {

		//memory not initialized
		DWORD oldProtect = 0;
		if (!VirtualProtect(baseAddress, SHARED_MEMORY_SIZE, PAGE_EXECUTE_READWRITE, &oldProtect)) {
			l.error(L"VirtualProtect");
			return;
		}

		//add waterMark
		ZeroMemory(baseAddress, SHARED_MEMORY_SIZE);
		if (!memcpy(baseAddress, SHARED_MEMORY_CHECK, sizeof(SHARED_MEMORY_CHECK))) {
			l.error(L"memcpy ShareMemory");
			return;
		}
	}
}

SharedMemory::~SharedMemory(){
}

DWORD SharedMemory::save(int apiName, DWORD data) {
	void* offset = (char*)baseAddress + (apiName * SHARED_MEMORY_CHUNK_SIZE);
	
	if (!memcpy(offset, (void*)&data, SHARED_MEMORY_CHUNK_SIZE)) {
		l.error(L"memcpy");
		return 0;
	}
	return 1;
}

DWORD SharedMemory::getData(DWORD apiName) {
	DWORD off = 0;
	void* ret = (char*)baseAddress + (apiName * SHARED_MEMORY_CHUNK_SIZE);
	if (!memcpy((void*)&off, ret, 4)) {
		l.error(L"memcpy SharedMemory.getData");
		return 0;
	}
	return off;
}
