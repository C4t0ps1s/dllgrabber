#pragma once

#include <vector>
#include <tuple>

#define COLLECTOR_ADDR 1
#define SOCKET_LIST_ADDR 2

#define PR_WRITE 4
#define PK_CIPHER_OP 8
#define PR_OPENTCPSOCKET 12
#define PR_CLOSE 16
#define PR_READ 20

#define HOOK 0
#define DEPORTED 1
#define ORIGIN 2

#define PROTOCOL_ERROR 1
#define PROTOCOL_SPDY 2
#define PROTOCOL_HTTP 4
#define METHOD_GET 8
#define METHOD_POST 16

#define DATA_TYPE_JSON 0
#define DATA_TYPE_URL_ENCODED 1

#define STATE_ERROR 0
#define STATE_USED 1
#define STATE_CLOSED 2

#define SHARED_MEMORY_CHECK "xxxx"
#define SHARED_MEMORY_SIZE 0x90
#define SHARED_MEMORY_CHUNK_SIZE 4

#define LOG_FILENAME L"log.txt"

using namespace std;

typedef struct struct_hook_param {
	//jump chain address
	void* originAPIAddress;
	void* hookedAPIAddress;
	void* deportedAPIAddress;

	//old memory protection
	int memoryProtection;

	//buff to save old overwritten shellcode
	char buffOriginCode[16];
	char buffHookedCode[16];

	//number of Bytes overwritten for hook
	int hookSize;

	//name of hook
	int name;
}struct_hook_param;

typedef struct dataBlob {
	char *data;
	int size;
	int type;
}dataBlob;

/**************************
* Internal NSS structures *
**************************/

typedef struct PRFileDesc {
	void* methods;
	void* secret;
	void* lower;
	void* higher;
	void* dtor;
	int identity;
}PRFileDesc;

/**************************
*    END NSS structures   *
**************************/

typedef struct s_request {
	char* data;
	DWORD size;
	DWORD protocol;
	DWORD method;
	bool analysed;
}s_request;

typedef struct sockStream {
	PRFileDesc* fd;
	vector<s_request> stream;
	//char* -> rawPacket
	//DWORD -> size
	//DWORD -> requestType
	int state; //Using, Wait for close
}sockStream;

