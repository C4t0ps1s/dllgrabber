#pragma once

#include <Windows.h>
#include <vector>

#include "const.h"
#include "Logger.h"
#include "SharedMemory.h"
#include "Collector.h"
#include "Socket.h"

class HookFunction
{
public:
	HookFunction();
	~HookFunction();

	DWORD __cdecl hook_PR_Write(PRFileDesc* fd, char* buff, DWORD size);
	DWORD __cdecl hook_PR_Read(PRFileDesc* fd, char* buff, DWORD size);
	DWORD __cdecl hook_PR_OpenTCPSocket(PRFileDesc* fd);
	DWORD __cdecl hook_PR_Close(PRFileDesc* fd);
	/*DWORD __cdecl hook_PK11_CipherOp(void* context, unsigned char* out, int* outlen, int maxout, char* in, int inlen);*/


private:
	LPVOID addrHookFunction;
	Logger l;
	vector<struct_hook_param> hookList;
	SharedMemory sm;
};