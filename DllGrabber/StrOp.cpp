#include "StrOp.h"



StrOp::StrOp()
{
}


StrOp::~StrOp()
{
}

string StrOp::remove(string input, char byte) {
	string s = "";
	for (DWORD i = 0; i < input.size(); i++) {
		if (input[i] != byte) {
			s += input[i];
		}
	}
	return s;
}

DWORD StrOp::split(string input, char separator, vector<string>* output) {

	DWORD cur = 0;
	while (cur < input.size()) {
		DWORD off = input.find(separator, cur);
		if (off == string::npos) {
			off = input.size();
		}

		string a = input.substr(cur, off - cur);
		output->push_back(a);
		cur = off + 1;
	}

	return 1;
}

string StrOp::toLower(string* input) {
	for (DWORD i = 0; i < input->size(); i++) {
		if (input->at(i) >= 'A' && input->at(i) <= 'Z') {
			input->at(i) += 0x20;
		}
	}
	return *input;
}

char* StrOp::find(char* data, char* substring, DWORD sizeData, DWORD sizeSubString) {
	
	//check if data are not NULL
	if (!data || !substring || !sizeData || !sizeSubString) {
		return NULL;
	}

	//looking for first occurence
	for (DWORD i = 0; i < sizeData; i++) {
		if (data[i] == substring[0]) {
			bool match = true;

			//check size
			if (i + sizeSubString > sizeData) {
				match = false;
				return NULL;
			}

			//check substring
			for (DWORD j = 0; j < sizeSubString; j++) {
				
				//reach end of data
				if (i + j > sizeData) {
					return NULL;
				}

				//string not found, 
				if (data[i + j] != substring[j]) {
					match = false;
					break;
				}
			}

			//found string, send absolute offset
			if (match) {
				return data + i;
			}
		}
	}
	return NULL;
}

char* StrOp::rfind(char* data, char* substring, DWORD sizeData, DWORD sizeSubString) {

	//check if data are not NULL
	if (data <= 0 || substring <= 0 || sizeData == 0 || sizeSubString == 0) {
		return NULL;
	}

	//looking for first occurence
	for (DWORD i = sizeData - 1; i >= 0; i--) {
		if (data[i] == substring[0]) {
			bool match = true;

			//check size
			if (i + sizeSubString > sizeData) {
				match = false;
				return NULL;
			}

			//compare substring
			for (DWORD j = 0; j < sizeSubString; j++) {
				if (i + j < sizeData && data[i + j] != substring[j]) {
					match = false;
				}
			}
			if (match) {
				return data + i;
			}
		}
	}
	return NULL;
}

string StrOp::initString(char* data, DWORD size) {
	string s = "";
	for (DWORD i = 0; i < size; i++) {
		if (data[i] > 0x00) {
			s += data[i];
		}
	}
	return s;
}