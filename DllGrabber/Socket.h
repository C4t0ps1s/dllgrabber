#pragma once

#include <Windows.h>
#include <vector>
#include <map>

#include "const.h"
#include "Logger.h"
#include "RequestParser.h"

using namespace std;

class Socket
{
public:
	Socket();
	~Socket();
	DWORD addSocket(PRFileDesc* fd);
	DWORD delSocket(PRFileDesc* fd);
	DWORD setCloseState(PRFileDesc* fd);
	DWORD getState(PRFileDesc* fd);
	vector<PRFileDesc*> getDescriptors();
	int isListened(PRFileDesc* fd);
	DWORD addStream(PRFileDesc* fd,char* buff,int size);
	vector<s_request>* getStream(PRFileDesc* fd);

private:
	map<PRFileDesc*,sockStream> socketList;
	Logger l;
	CRITICAL_SECTION cs;
	RequestParser* rp;

	DWORD concatPostData(s_request* request, char* buff, int size);
};

