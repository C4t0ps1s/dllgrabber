#pragma once

#include <windows.h>
#include <vector>

#include "Collector.h"
#include "Engine.h"
#include "HookFunction.h"
#include "const.h"
#include "Logger.h"
#include "SharedMemory.h"


#define sizeBuff 0x500

class Hook
{
public:
	Hook(Collector* c);
	~Hook();
	int setHook();

private:
	Engine e;
	Collector* c;

	LPVOID tramp;
	DWORD sizeTramp;

	Logger l;
	vector<struct_hook_param> hookList;
	SharedMemory sm;

	DWORD initHookTable();
	DWORD initHook(TCHAR* dllName, char* API, int offset,int hookSize);

};

