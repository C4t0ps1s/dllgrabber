#include "Collector.h"


Collector::Collector(){
	InitializeCriticalSection(&cs);
}


Collector::~Collector(){
}

wstring Collector::getPath(){
	return pathDump;
}

void Collector::addData(PRFileDesc* fd) {

	EnterCriticalSection(&cs);
	q.push(fd);
	LeaveCriticalSection(&cs);
	return;
}

void Collector::startWorker(){
	wp.l = &this->l;
	wp.queueAddr = getQueueAddress();
	CreateThread(NULL, 0,&threadWorker,&wp, 0, NULL);
}

DWORD WINAPI threadWorker(LPVOID lpParam){
	workerParam* wp = (workerParam*)lpParam;
	queue<PRFileDesc*>* q = (queue<PRFileDesc*>*)wp->queueAddr;
	Logger* l = (Logger*)wp->l;
	RequestParser rp(l);
	SharedMemory sm;
	Socket* s = (Socket*)sm.getData(SOCKET_LIST_ADDR);
	CRITICAL_SECTION cs;
	InitializeCriticalSection(&cs);
	

	while (true){

		vector<s_request>* stream = NULL;
		auto descriptorList = s->getDescriptors();

		for (auto descriptor = descriptorList.begin(); descriptor != descriptorList.end(); descriptor++) {
			stream = s->getStream(*descriptor);

			//analyse new packets
			if (stream) {
				for (DWORD i = 0; i < stream->size(); i++){
					if (!stream->at(i).analysed) {
						rp.parseRaw(&stream->at(i));
					}
				}
			}	
		}

		//Close 
		descriptorList = s->getDescriptors();
		auto descriptor = descriptorList.begin();
		while(descriptor != descriptorList.end()){
			if (s->getState(*descriptor) == STATE_CLOSED) {
				s->delSocket(*descriptor);
			}
			descriptor++;
		}
		Sleep(50);
	}

	while (true) {
		
	}
	
	return 1;
}

int Collector::size(){
	return q.size();
}

DWORD Collector::getQueueAddress() {
	return (DWORD)&q;
}