#include "Hook.h"

using namespace std;

Hook::Hook(Collector* c){
	this->c = c;
	sizeTramp = 0;
}

Hook::~Hook(){
}

int Hook::setHook(){
	
	initHookTable();
	HookFunction hf;

	for (auto it = hookList.begin(); it < hookList.end(); it++) {

		//save origin prologue
		if (!memcpy(it->buffOriginCode, it->originAPIAddress, it->hookSize)) {
			return 0;
		}

		//set prologue in deportedAPI
		if (!memcpy(it->deportedAPIAddress, it->buffOriginCode, it->hookSize)) {
			return 0;
		}

		//compute jumper deportedAPI -> originAPI + hookSize
		DWORD diff2 = (DWORD)it->originAPIAddress + it->hookSize - ((DWORD)it->deportedAPIAddress + it->hookSize) - 5;
		char jump1[16] = { 0 };

		jump1[0] = '\xe9'; //relative jump
		*(int*)(jump1 + 1) = (int)diff2;

		//fill padding with nop opcode
		for (int i = 5; i < it->hookSize; i++) { jump1[i] = '\x90';}

		//set jumper deportedAPI -> originAPI + hookSize
		if (!memcpy((void*)((DWORD)it->deportedAPIAddress + it->hookSize), jump1, it->hookSize)) {
			return 0;
		}

		//compute jumper originAPI -> hookAPI
		DWORD diff = (DWORD)it->hookedAPIAddress - (DWORD)it->originAPIAddress - 5;
		char jump2[16] = { 0 };
		jump2[0] = '\xe9'; //relative jump
		*(int*)(jump2 + 1) = (int)diff;
		//fill padding with nop opcode
		for (int i = 5; i < it->hookSize; i++) { jump2[i] = '\x90'; }
		if (!memcpy(it->buffHookedCode, jump2, it->hookSize)) {
			return 0;
		}

		//set temporary RWX protection on originAPI
		DWORD oldProtect;
		VirtualProtect(it->originAPIAddress, 0x100, PAGE_EXECUTE_READWRITE, &oldProtect);
		it->memoryProtection = oldProtect;

		//set hook originAPI -> hookAPI
		if (!memcpy(it->originAPIAddress, it->buffHookedCode, it->hookSize)) {
			return 0;
		}

		//set old protection
		VirtualProtect(it->originAPIAddress, 0x100, it->memoryProtection, &oldProtect);
	}
	return true;
}

DWORD Hook::initHook(TCHAR* dllName, char* API,int offset, int hookSize) {
	struct_hook_param param;

	HMODULE hm = GetModuleHandle(dllName);
	param.originAPIAddress = GetProcAddress(hm,API);
	if (!param.originAPIAddress) {
		l.error(l.a2w(string(API))+L" not found");
		return 0;
	}
	else {
		l.info(l.a2w(string(API)) + L" found");
		l.info(L"[*] OriginAPI:   0x" + l.i2w((DWORD)param.originAPIAddress));
	}

	param.hookSize = hookSize;
	param.hookedAPIAddress = (void*)sm.getData(offset + HOOK);
	l.info(L"[*] hookedAPI:   0x" + l.i2w((DWORD)param.hookedAPIAddress));
	param.deportedAPIAddress = VirtualAlloc(NULL, 0x20, MEM_COMMIT, PAGE_EXECUTE_READWRITE);
	if (!param.deportedAPIAddress) {
		l.error(L"Fail virtualAlloc");
		return 0;
	}
	else {
		l.info(L"[*] DeportedAPI: 0x" + l.i2w((DWORD)param.deportedAPIAddress));
	}
	sm.save(offset + DEPORTED, (DWORD)param.deportedAPIAddress);
	hookList.push_back(param);
	return 1;
}

DWORD Hook::initHookTable() {
	initHook(L"nss3.dll", "PR_Write", PR_WRITE,8);
	initHook(L"nss3.dll", "PR_OpenTCPSocket", PR_OPENTCPSOCKET,8);
	initHook(L"nss3.dll", "PR_Close", PR_CLOSE, 7);
//	initHook(L"nss3.dll", "PR_Read", PR_READ, 8);
	return 1;
}