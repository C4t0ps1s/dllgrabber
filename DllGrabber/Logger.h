#pragma once

#include <windows.h>
#include <string>
#include <iostream>
#include <math.h>
#include <sstream>

#include "Logger.h"
#include "const.h"

using namespace std;

class Logger
{
private:
	int level;
	CRITICAL_SECTION cs;
	void print(wstring data);
	void init(int level);
	wstring path;

public:
	Logger();
	~Logger();

	Logger(int level);
	void setLevel(int level);
	int getLevel();
	
	string w2a(wstring ws);
	wstring a2w(string as);
	wstring i2w(DWORD value);

	void error(wstring log);
	void info(wstring log);
	void debug(wstring log);

	void error(string log);
	void info(string log);
	void debug(string log);
};

