#include "Socket.h"



Socket::Socket(){
	rp = new RequestParser(&l);
	InitializeCriticalSection(&cs);
}


Socket::~Socket(){
	delete rp;
}

DWORD Socket::addSocket(PRFileDesc* fd) {
	sockStream ss;
	ss.fd = fd;
	ss.state = STATE_USED;
	socketList[fd] = ss;
	//l.info(L"fd 0x"+l.i2w((DWORD)fd) + L" Added (s: 0x"+l.i2w((DWORD)socketList.size())+L")");
	return 1;
}

DWORD Socket::getState(PRFileDesc* fd) {
	if (!isListened(fd)) {
		return STATE_ERROR;
	}
	return socketList[fd].state;
}

vector<PRFileDesc*> Socket::getDescriptors() {
	vector<PRFileDesc*> ret;
	for (auto it = socketList.begin(); it != socketList.end(); it++) {
		//if (it->second.state != STATE_CLOSED) {
			ret.push_back(it->first);
		//}
		
	}
	return ret;
}

DWORD Socket::setCloseState(PRFileDesc* fd) {
	if (!isListened(fd)) {
		return 0;
	}

	socketList[fd].state = STATE_CLOSED;
	return 1;
}

DWORD Socket::delSocket(PRFileDesc* fd) {
	if (!isListened(fd)) {
		return 0;
	}
	//clean allocations
	auto ss = socketList.find(fd);
	auto stream = ss->second.stream;
	//EnterCriticalSection(&cs);
	//l.info(L"Try to remove fd 0x" + l.i2w((DWORD)fd));
	for (auto it = stream.begin(); it < stream.end(); it++) {
		free(it->data);
	}
	socketList.erase(fd);
	//LeaveCriticalSection(&cs);
	//l.info(L"fd 0x" + l.i2w((DWORD)fd) + L" removed (s: 0x" + l.i2w((DWORD)socketList.size()) + L")");
	return 1;
}

int Socket::isListened(PRFileDesc* fd) {
	if (socketList.find(fd) == socketList.end()) {
		return 0;
	}
	return 1;
}

DWORD Socket::addStream(PRFileDesc* fd, char* buff, int size) {
	int pos = isListened(fd);
	if (pos == -1){ 
		return 0;
	}

	//copy it
	char* tmp = (char*)malloc(size);
	if (!tmp) {
		l.error(L"malloc in AddStream");
		return 0;
	}

	if (!memcpy(tmp, buff, size)) {
		l.error(L"memcpy in AddStream");
		return 0;
	}

	//get requestType
	DWORD rt = rp->getRequestType(buff, size);

	//save stream
	s_request r;
	r.data = tmp;
	r.size = size;
	r.protocol = rt & (PROTOCOL_SPDY | PROTOCOL_HTTP);
	r.method = rt & (METHOD_GET | METHOD_POST);
	r.analysed = false;

	if (rt == PROTOCOL_ERROR) {
		
		//append to POST packet
		if (socketList[fd].stream.size() > 0 && socketList[fd].stream.back().method == METHOD_POST) {

			//append current packet to previous if stack not empty
			if (!socketList[fd].stream.empty()) {
				concatPostData(&socketList[fd].stream.back(), buff, size);
				free(tmp);
			}
		}
			//add it in stack otherwise
		else {
			EnterCriticalSection(&cs);
			socketList[fd].stream.push_back(r);
			LeaveCriticalSection(&cs);
		}
		
	}

	//formated packet
	else {
		EnterCriticalSection(&cs);
		socketList[fd].stream.push_back(r);
		LeaveCriticalSection(&cs);
	}

	return 1;
}

vector<s_request>* Socket::getStream(PRFileDesc* fd) {
	if (isListened(fd)) {
		return &socketList[fd].stream;
	}
	else {
		return NULL;
	}	
}

DWORD Socket::concatPostData(s_request* request, char* buff, int size) {
	DWORD newSize = request->size + size;
	char* newData = (char*)malloc(newSize);
	memcpy(newData, request->data, request->size);
	memcpy(newData + request->size, buff, size);
	char* tmp = request->data;
	request->data = newData;
	request->size = newSize;
	request->analysed = false;
	free(tmp);
	return 1;
}

