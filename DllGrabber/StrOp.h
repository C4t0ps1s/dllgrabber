#pragma once

#include <Windows.h>
#include <string>
#include <vector>

using namespace std;

class StrOp
{
public:
	StrOp();
	~StrOp();

	//String operations
	string initString(char* data, DWORD size);
	string remove(string input, char byte);
	DWORD split(string input, char separator, vector<string>* output);
	string toLower(string* input);
	char* find(char* data, char* substring, DWORD sizeData, DWORD sizeSubString);
	char* rfind(char* data, char* substring, DWORD sizeData, DWORD sizeSubString);
};

