#include "Logger.h"

Logger::Logger(){
	init(3);
}

Logger::Logger(int l){
	init(l);
}

void Logger::init(int l) {
	this->level = l;
	InitializeCriticalSection(&cs);
	DWORD buffSize = GetEnvironmentVariable(L"USERPROFILE", NULL, 0);
	TCHAR* buff = (TCHAR*)malloc(sizeof(WCHAR)*buffSize);
	if (buff) {
		GetEnvironmentVariable(L"USERPROFILE", (LPWSTR)buff, buffSize);
		path = (TCHAR*)buff + wstring(L"\\Desktop\\") + LOG_FILENAME;
		free(buff);
	}
	else {
		path = L"C:\\";
	}
	
}

Logger::~Logger(){
}

// 0 -> no output
// 1 -> [Error]
// 2 -> [Error] + [Info]
// 3 -> [Error] + [Info] + [Debug]
void Logger::setLevel(int level){
	this->level = level;
}

int Logger::getLevel(){
	return this->level;
}

/**************
*   Unicode   * 
**************/
void Logger::error(wstring log){
	DWORD last = GetLastError();
	if (level > 0){
		print(L"[Error " + to_wstring(last) + L"]" + log);
	}
}

void Logger::info(wstring log){
	if (level > 1){
		print(L"[Info]" + log);
	}
}

void Logger::debug(wstring log){
	if (level > 2){
		print(L"[Debug]" + log);
	}
}

/************
*   ANSI    *
************/
void Logger::error(string log) {
	error(a2w(log));
}

void Logger::info(string log) {
	info(a2w(log));
}
void Logger::debug(string log) {
	debug(a2w(log));
}

void Logger::print(wstring data){

	wstring out = L"[" + to_wstring(GetCurrentProcessId()) + L"-" + to_wstring(GetCurrentThreadId()) + L"]";
	int space = 14 - out.size();
	for (int i = 0; i < space; i++) {
		out += L" ";
	}
	out += data + L"\r\n";
	EnterCriticalSection(&cs);
	HANDLE hFile = CreateFile(path.c_str(), GENERIC_WRITE, FILE_SHARE_WRITE | FILE_SHARE_READ, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	if (hFile <= 0){
		DWORD err = GetLastError();
		wcout << out;
	}
	else{
		
		SetFilePointer(hFile, 0, NULL, FILE_END);
		DWORD written = 0;
		WriteFile(hFile, out.c_str(), out.length() * 2, &written, NULL);
		CloseHandle(hFile);
	}
	LeaveCriticalSection(&cs);
	return;
}

string Logger::w2a(wstring ws){
	string out = string(ws.begin(), ws.end());
	return out;
}

wstring Logger::a2w(string as){
	wstring out = wstring(as.begin(), as.end());
	return out;
}

wstring Logger::i2w(DWORD value) {
	wstringstream stream;
	stream << std::hex << value;
	return stream.str();
}