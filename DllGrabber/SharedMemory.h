#pragma once

#include <Windows.h>

#include "Logger.h"
#include "const.h"

class SharedMemory
{
public:
	SharedMemory();
	~SharedMemory();
	DWORD save(int offset, DWORD data);
	DWORD getData(DWORD apiName);

private:
	HMODULE hCurrentDll;
	void* baseAddress;
	Logger l;
	//DWORD addrLastBlock;
};

