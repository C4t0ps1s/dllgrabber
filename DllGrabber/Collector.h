#pragma once
#include <windows.h>
#include <queue>

#include "Logger.h"
#include "Engine.h"
#include "RequestParser.h"
#include "Socket.h"
#include "SharedMemory.h"

typedef struct workerParam {
	DWORD queueAddr;
	Logger* l;
}workerParam;

using namespace std;

class Collector
{
public:
	Collector();
	~Collector();
	//void addData(char* data, int size, int type);
	void addData(PRFileDesc* fd);
	void startWorker();
	int size();
	wstring getPath();
	DWORD getQueueAddress();

private:
	Engine e;
	queue<PRFileDesc*> q;
	wstring pathDump;
	Logger l;
	workerParam wp;
	CRITICAL_SECTION cs;
};

DWORD WINAPI threadWorker(LPVOID lpParam);





