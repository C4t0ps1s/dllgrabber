#include "Engine.h"


Engine::Engine(){
}


Engine::~Engine(){
}

DWORD64 Engine::w2i(wstring input){

	DWORD64 ret = 0;
	for (unsigned int i = 0; i < input.size(); i++){
		TCHAR tmp = input[input.size() - i - 1];
		if (tmp >= L'0' && tmp <= L'9'){
			tmp -= 0x30;
			ret += (DWORD64)(tmp*pow(10, i));
		}
		else{
			return -1;
		}
	}
	return ret;
}

wstring Engine::dec2hex(wstring decString){
	DWORD64 tmp = w2i(decString);
	wstringstream ss;
	ss << L"0x" << hex << tmp;
	return wstring(ss.str());
}

wstring Engine::dec2hex(LPVOID decValue){
	wstringstream ss;
	ss << L"0x" << hex << decValue;
	return wstring(ss.str());
}

wstring Engine::dec2hex(DWORD decValue){
	wstringstream ss;
	ss << L"0x" << hex << decValue;
	return wstring(ss.str());
}

wstring Engine::s2w(string str){
	wstring ret = L"";
	for (unsigned int i = 0; i < str.size(); i++){
		ret += (TCHAR)str.at(i);
	}
	return ret;
}

wstring Engine::i2w(int input){
	return to_wstring(input);
}

wstring Engine::i2w(DWORD64 input){
	return to_wstring(input);
}