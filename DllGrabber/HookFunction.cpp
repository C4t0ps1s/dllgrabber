#include "HookFunction.h"



HookFunction::HookFunction(){
}


HookFunction::~HookFunction(){
}

DWORD __cdecl HookFunction::hook_PR_Read(PRFileDesc* fd, char* buff, DWORD size) {

	Collector* c = (Collector*)sm.getData(COLLECTOR_ADDR);
	//check if socket is in list
	Socket* s = (Socket*)sm.getData(SOCKET_LIST_ADDR);
	if (!s->isListened((PRFileDesc*)fd)) {
		return 1;
	}
	//l.debug(L"PR_Read fd: 0x" + l.i2w((DWORD)fd) + L" size: 0x" + l.i2w(size));
	return 1;
}

DWORD __cdecl HookFunction::hook_PR_Write(PRFileDesc* fd, char* buff, DWORD size){
	
	Collector* c =  (Collector*)sm.getData(COLLECTOR_ADDR);
	//check if socket is in list
	Socket* s = (Socket*)sm.getData(SOCKET_LIST_ADDR);
	if (!s->isListened((PRFileDesc*)fd)) {
		return 1;
	}
	s->addStream(fd, buff, size);
	//c->addData(fd);
	return 1;
}

DWORD __cdecl HookFunction::hook_PR_OpenTCPSocket(PRFileDesc* fd) {

	Socket* s = (Socket*)sm.getData(SOCKET_LIST_ADDR);
	s->addSocket(fd);
	return 1;
}

DWORD __cdecl HookFunction::hook_PR_Close(PRFileDesc* fd) {
	Socket* s = (Socket*)sm.getData(SOCKET_LIST_ADDR);
	s->setCloseState(fd);
	return 1;
}

//DWORD __cdecl HookFunction::hook_PK11_CipherOp(void* context, unsigned char* out, int* outlen, int maxout, char* in, int inlen) {
//
//	//Keep just encrypt call
//	DWORD* contextValue = (DWORD*)context;
//	if (*contextValue != 0x104) {
//		return 1;
//	}
//	int type = PROTOCOL_SPDY;
//	Collector* c = (Collector*)sm.getData(COLLECTOR_ADDR);
//	if (!strncmp(in, "GET", 3) || !strncmp(in, "POST", 4)) {
//		type = PROTOCOL_HTTP;
//	}
//	c->addData(in,inlen,type);
//	return 1;
//}